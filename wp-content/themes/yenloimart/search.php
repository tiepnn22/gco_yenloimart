<?php get_header(); ?>

<?php
	$s = $_GET['s'];
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="green-pro">
    <div class="container">
        <h1 class="s24 text-center pb-4 contact-tit">Kết quả tìm kiếm cho : [<?php echo $s; ?>]</h1>

        <div class="">
            <div class="row justify-content-between ptab">

                <?php
                    $query = query_search_post_paged($s, array('product'), 4);
                    $max_num_pages = $query->max_num_pages;

                    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                ?>

                    <?php get_template_part('resources/views/content/category-product', get_post_format()); ?>

                <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

            </div>

            <!--pagination-->
            <?php echo paginationCustom( $max_num_pages ); ?>
        </div>

    </div>
</section>

<?php get_footer(); ?>