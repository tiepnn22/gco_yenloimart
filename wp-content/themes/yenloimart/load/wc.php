<?php
// Check price
if (!function_exists('check_price_old_price')) {
    function check_price_old_price($post_id, &$post_link, &$old_price, &$price, &$type, &$stock) {
        $post_id    = '' ? get_the_ID() : $post_id;
        $post_link  = get_permalink($post_id);
        $product    = wc_get_product( $post_id );

        $type       = $product->is_type( 'variable' ); // check variable
        $stock      = $product->get_stock_quantity();

        if($type == true) {
            $count = count($product->get_available_variations());
            
            if($count == 1) { // có 1 variable sẽ ko có giá min max
                $old_price  = (float)$product->get_variation_regular_price();
                $price      = (float)$product->get_variation_sale_price();
            } else {
                $old_price  = (float)$product->get_variation_price(('max'));
                $price      = (float)$product->get_variation_price(('min'));
            }
        } else {
            $old_price      = (float)$product->get_regular_price();
            $price          = (float)$product->get_sale_price();
        }
    }
}


// Show Add_to_cart_button woo
// if (!function_exists('show_add_to_cart_button')) {
//     function show_add_to_cart_button($post_id) {
//         echo check_price_old_price($post_id, $post_link, $old_price, $price, $type, $stock);
//         $str = "";

//         if($type >= 1) {
//             $str .= '
//                 <a href="'.$post_link.'">
//                     Mua hàng
//                 </a>';
//         } else {
//             if($old_price > 0){
//                 if(!isset($stock) || $stock > 0){
//                     $str .= '
//                     <div class="product__control" style="transform: translateY(0);">
//                         <form class="cart" action="'.wc_get_cart_url().'" method="post" enctype="multipart/form-data">
//                             <button type="submit" name="add-to-cart" value="'.esc_attr( $post_id ).'" class="single_add_to_cart_button button alt">
//                                 Mua hàng
//                             </button>
//                         </form>
//                     </div>';
//                 } else {
//                     $str .= 'Hết hàng';
//                 }
//             } else {
//                 $str .= '';
//             }
//         }
//         return $str;
//     }
// }
// Show Add_to_cart_button woo ajax
if (!function_exists('show_add_to_cart_button_ajax')) {
    function show_add_to_cart_button_ajax($post_id) {
        echo check_price_old_price($post_id, $post_link, $old_price, $price, $type, $stock);
        $str = "";

        if($type >= 1) {
            $str .= '
                <a class="btn buy-btn" href="'.$post_link.'">
                    Mua hàng
                </a>';
        } else {
            if($old_price > 0){
                if(!isset($stock) || $stock > 0){
                    $str .= '
                    <a href="javascript:void(0)" value="0" class="ajax_add_to_cart add_to_cart_button btn buy-btn" data-product_id="'.$post_id.'" data-product_sku="">
                        Mua hàng
                    </a>';
                } else {
                    $str .= 'Hết hàng';
                }
            } else {
                $str .= '';
            }
        }
        return $str;
    }
}
// Show Add_to_cart button + quantity woo
if (!function_exists('show_add_to_cart_button_quantity')) {
    function show_add_to_cart_button_quantity($post_id) {
        echo check_price_old_price($post_id, $post_link, $old_price, $price, $type, $stock);
        $str = "";

        if($type >= 1) {
            $str .= '
                <a href="'.$post_link.'">
                    Mua hàng
                </a>';
        } else {
            if($old_price > 0){
                if(!isset($stock) || $stock > 0){
                    $str .= '
                    <form class="cart" action="'.wc_get_cart_url().'" method="post" enctype="multipart/form-data">
                        <div class="quantity">
                            <input type="number" id="quantity_60afce1e20112" class="input-text qty text" step="1" min="1" max="'.$stock.'" name="quantity" value="1" title="Qty" size="4" placeholder="" inputmode="numeric">
                        </div>
                        <button type="submit" name="add-to-cart" value="'.$post_id.'" class="single_add_to_cart_button button alt">
                            Mua hàng
                        </button>
                    </form>';
                } else {
                    $str .= 'Hết hàng';
                }
            } else {
                $str .= '';
            }
        }
        return $str;
    }
}
// Show Rating woo
// if (!function_exists('show_rating')) {
//     function show_rating($post_id) {
//         $post_id = '' ? get_the_ID() : $post_id;
//         // global $product;
//         $product = new WC_product($post_id);
//         $str = "";

//         if($ave = $product->get_average_rating($post_id)) {
//             $ave_check = $ave/5*100;
        
//             $str .= '
//             <div class="woocommerce-product-rating">
//                 <div class="star-rating" role="img" aria-label="Được xếp hạng '.$ave.' trên 5 sao">
//                     <span style="width:'.$ave_check.'%">
//                     </span>
//                 </div>
//             </div>';
//         } else {
//             $str .= '
//             <div class="woocommerce-product-rating">
//                 <div class="star-rating" role="img" aria-label="Được xếp hạng 5 trên 5 sao">
//                 </div>
//             </div>';
//         }
//         return $str;
//     }
// }
// Show Attributes woo (name)
// if (!function_exists('show_attributes')) {
//     function show_attributes($post_id) {
//         $post_id      = '' ? get_the_ID() : $post_id;
//         $product_attr = get_post_meta( $post_id, '_product_attributes' );
//         $str = "";

//         $str .= '<ul class="vk-shop-detail__list">';

//         foreach($product_attr as $attr) {
//             foreach($attr as $attribute) {
//                 $attr_name   = wc_attribute_label($attribute['name']);
//                 $attrvalue   = array( wc_get_product_terms( $post_id, $attribute['name'], array( 'fields' => 'names' ) ) );
//                 // $attrvalue   = wc_get_product_terms( $post_id, $attribute['name'] ); // info attribute
//                 $attr_values = implode(", ", $attrvalue[0]);

//                 $str .= '<li>'.$attr_name.': <strong>'.$attr_values.'</strong></li>';
//             }
//         }

//         $str .= '</ul>';

//         return $str;
//     }
// }


// Format price
// if (!function_exists('format_price')) {
//     function format_price($money) {
//         $str = "";
        
//         if($money != 0) {
//             $num = (float)$money;
//             $str = number_format($num,0,'.','.');
//         }
//         return $str;
//     }
// }
// Format price + VNĐ
if (!function_exists('format_price_donvi')) {
    function format_price_donvi($money) {
        $donvi = ' '.get_woocommerce_currency_symbol();
        $str = "";

        if($money != 0) {
            $num = (float)$money;
            $str = number_format($num,0,'.','.');
            $str .= $donvi;
            $str = $str;
        }
        return $str;
    }
}
// Check old_price, price + VNĐ
if (!function_exists('show_price_old_price')) {
    function show_price_old_price($post_id) {
        echo check_price_old_price($post_id, $post_link, $old_price, $price, $type, $stock);
        $str = "";

        if($old_price > 0) {
            if($price > 0 || $price != null) {
                $str1 = format_price_donvi($price);
                $str2 = format_price_donvi($old_price);
                $str = '<h4 class="s16 t2 medium sale-item-price">
                            <span class="price-news">'.$str1.'</span>
                            <del class="price-old t5">'.$str2.'</del>
                        </h4>';
            } else {
                $str = '<h4 class="s16 t2 medium sale-item-price">
                            <span class="price-news">'.format_price_donvi($old_price).'</span>
                        </h4>';
            }
        } else {
            $str = '<h4 class="s16 t2 medium sale-item-price">
                        <a class="price-news" href="'.get_link_page_template('template-contact.php').'">Liên hệ</a>
                    </h4>';
        }
        return $str;
    }
}
// Show % sale price
// if (!function_exists('show_sale')) {
//     function show_sale($post_id) {
//         echo check_price_old_price($post_id, $post_link, $old_price, $price, $type, $stock);
//         $str = "";

//         if($old_price == 0){ } else {
//             if($price == 0){ } else {
//                 $sale = (1 - ($price / $old_price))*100;
//                 $str = '<span class="price-sale">'.ceil($sale).'%</span>';
//             }
//         }
//         return $str;
//     }
// }


// Change select attribute to radio
// add_action( 'woocommerce_after_single_product', function() {
//     global $product;
//     if( ! $product || ! $product->is_type( 'variable' ) ) {
//         return;
//     }

//     <script>
//         jQuery( document ).ready( function( $ ) {
//             $( ".variations_form" )
//             .on( "wc_variation_form woocommerce_update_variation_values", function() {
//                 $( "label.generatedRadios" ).remove();
//                 $( "table.variations select" ).each( function() {
//                     var selName = $( this ).attr( "name" );
//                     $( "select[name=" + selName + "] option" ).each( function() {
//                         var option = $( this );
//                         var value = option.attr( "value" );
//                         if( value == "" ) { return; }
//                         var label = option.html();
//                         var select = option.parent();
//                         var selected = select.val();
//                         var isSelected = ( selected == value )
//                             ? " checked=\"checked\"" : "";
//                         var selClass = ( selected == value )
//                             ? " selected" : "";
//                         var radHtml
//                             = `<input name="${selName}" ${isSelected} type="radio" value="${value}" />`;
//                         var optionHtml
//                             = `<label class="generatedRadios${selClass}">${radHtml} ${label}</label>`;
//                         select.parent().append(
//                             $( optionHtml ).click( function() {
//                                 select.val( value ).trigger( "change" );
//                             } )
//                         )
//                     } ).parent().hide();
//                 } );
//             } );
//         } );
//     </script>

// } );


// // Sửa nút đặt hàng page checkout (ko dung đc icl) (css)
// add_filter( 'woocommerce_order_button_text', 'checkout_button_text_translation' );
// function checkout_button_text_translation( $button_text ) {
//     $button_text = 'Gửi';
//     return $button_text;
// }


// // Sửa thông báo Delete sản phẩm page Cart (ko dung đc icl) (css)
// add_filter( 'woocommerce_cart_item_removed_title', 'cart_remove_text_translation', 12, 2);
// function cart_remove_text_translation( $message, $cart_item ) {
//     $product = wc_get_product( $cart_item['product_id'] );
//     if( $product )
//         // $message = '<div class="wc_delete_undo">Đã xoá sản phẩm '.$product->get_name().'</div>';
//         $message = '<div class="wc_delete_undo">Xóa sản phẩm thành công!</div>';

//     return $message;
// }
// add_filter('gettext', 'cart_undo_text_translation', 35, 3 );
// function cart_undo_text_translation( $translation, $text, $domain ) {
//     if( $text === 'Undo?' ) {
//         $translation = '<div class="wc_undo">Hoàn tác!</div>';
//     }
//     return $translation;
// }


// // Sửa thông báo Update sản phẩm page Cart (ko dung đc icl) (css)
// add_filter('gettext', 'cart_update_text_translation', 10, 3);
// function cart_update_text_translation($translation, $text, $domain) {
//     // if ($domain == 'woocommerce') {
//         if ($text == 'Cart updated.') {
//             $translation = '<div class="wc_updated">Cập nhật giỏ hàng thành công!</div>';
//         }
//     // }
//     return $translation;
// }


// // Sửa thông báo Giỏ hàng trống (ko dung đc icl) (css)
// add_filter('gettext', 'cart_empty_text_translation', 10, 3);
// function cart_empty_text_translation($translation, $text, $domain) {
//     // if ($domain == 'woocommerce') {
//         if ($text == 'Your cart is currently empty.') {
//             $translation = 'Giỏ hàng hiện đang trống!';
//         }
//     // }
//     return $translation;
// }
// add_filter('gettext', 'checkout_cart_empty_text_translation', 10, 3);
// function checkout_cart_empty_text_translation($translation, $text, $domain) {
//     // if ($domain == 'woocommerce') {
//         if ($text == 'Checkout is not available whilst your cart is empty.') {
//             $translation = 'Thanh toán không có sẵn trong khi giỏ hàng của bạn trống!';
//         }
//     // }
//     return $translation;
// }
// add_filter('gettext', 'checkout_empty_text_translation', 10, 3);
// function checkout_empty_text_translation($translation, $text, $domain) {
//     // if ($domain == 'woocommerce') {
//         if ($text == 'Sorry, your session has expired.') {
//             $translation = 'Xin lỗi, phiên của bạn đã hết hạn!';
//         }
//     // }
//     return $translation;
// }


// // Sửa thông báo Add vào giỏ hàng thành công
// add_filter('wc_add_to_cart_message', 'add_to_cart_text_translation', 10, 2);
// function add_to_cart_text_translation($message, $product_id) {
//     // if(ICL_LANGUAGE_CODE == 'vi'){
//     //     $message = 'Thêm vào giỏ hàng thành công !';
//     // } elseif (ICL_LANGUAGE_CODE == 'en') {
//     //     $message = 'Add to cart successfully !';
//     // }
//     $message = 'Thêm vào giỏ hàng thành công!';
//     return $message;
// }


// // Sửa thông báo Comment rỗng vì chưa chọn rating
// add_filter('gettext', 'comment_form_rating_text_translation', 10, 3);
// function comment_form_rating_text_translation($translation, $text, $domain) {
//     // if ($domain == 'woocommerce') {
//         if ($text == 'Please select a rating') {
//             $translation = 'Vui lòng chọn một đánh giá';
//         }
//     // }
//     return $translation;
// }


// // Sửa thông báo validation error form checkout (ko dung đc icl) (css)
// add_action( 'woocommerce_after_checkout_validation', 'checkout_validation_unique_error', 9999, 2 );
// function checkout_validation_unique_error( $data, $errors ){
//     // Check for any validation errors
//     if( ! empty( $errors->get_error_codes() ) ) {

//         // Remove all validation errors
//         foreach( $errors->get_error_codes() as $code ) {
//             $errors->remove( $code );
//         }

//         // Add a unique custom one
//         $checkout_validation = '<div class="wc_field_checkout">Trường được yêu cầu!</div>';
//         $errors->add( 'validation', $checkout_validation );
//     }
// }
// // Sửa thông báo validation error Email form checkout (ko dung đc icl) (css)
// add_filter('gettext', 'invalid_billing_email_text_translation', 10, 3);
// function invalid_billing_email_text_translation($translation, $text, $domain) {
//     // if ($domain == 'woocommerce') {
//         if ($text == 'Invalid billing email address') {
//             $translation = '<div class="wc_invalid_email">Địa chỉ e-mail không hợp lệ!</div>';
//         }
//     // }
//     return $translation;
// }


// // Sửa thông báo Bank page thankyou (ko dung đc icl) (css)
// add_filter('gettext', 'atm_bank_alert_text_translation', 10, 3);
// function atm_bank_alert_text_translation($translation, $text, $domain) {
//     // if ($domain == 'woocommerce') {
//         if ($text == 'Our bank details') {
//             $translation = 'Chuyển khoản vào tài khoản ngân hàng dưới đây !';
//         }
//     // }
//     return $translation;
// }
// // Sửa thông báo name Bank page thankyou (ko dung đc icl) (css)
// add_filter('gettext', 'bank_text_translation', 10, 3);
// function bank_text_translation($translation, $text, $domain) {
//     // if ($domain == 'woocommerce') {
//         if ($text == 'Bank') {
//             $translation = 'Ngân hàng';
//         }
//     // }
//     return $translation;
// }
// // Sửa thông báo account Bank page thankyou (ko dung đc icl) (css)
// add_filter('gettext', 'atm_bank_account_text_translation', 10, 3);
// function atm_bank_account_text_translation($translation, $text, $domain) {
//     // if ($domain == 'woocommerce') {
//         if ($text == 'Account number') {
//             $translation = 'Số tài khoản';
//         }
//     // }
//     return $translation;
// }


//Tuỳ chỉnh form checkout
function customCheckoutFields($fields) {
    // echo '<pre>';
    // var_dump($fields);
    // echo '</pre>';

    $fields['billing']['billing_country']['required'] = false;
    $fields['billing']['billing_city']['required'] = false;
    $fields['billing']['billing_state']['required'] = false;

    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_state']);

    unset($fields['billing']['billing_last_name']);
    // unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_company']);
    unset($fields['account']['account_password']);


    // if(ICL_LANGUAGE_CODE == 'vi'){
    //     $fields['billing']['billing_first_name']['label'] = __('Họ tên');
    // } elseif (ICL_LANGUAGE_CODE == 'en') {
    //     $fields['billing']['billing_first_name']['label'] = __('Name');
    // }
    $fields['billing']['billing_first_name']['label'] = __('Họ và tên');
    $fields['billing']['billing_first_name']['class'] = ['form-row-wide'];
    // $_POST['billing_first_name'] = '';
    // $fields['billing']['billing_first_name']['default'] = "";


    // if(ICL_LANGUAGE_CODE == 'vi'){
    //     $fields['billing']['billing_address_1']['label'] = __('Địa chỉ');
    // } elseif (ICL_LANGUAGE_CODE == 'en') {
    //     $fields['billing']['billing_address_1']['label'] = __('Address');
    // }
    $fields['billing']['billing_address_1']['label'] = __('Địa chỉ');
    $fields['billing']['billing_address_1']['class'] = ['form-row-wide'];
    $fields['billing']['billing_address_1']['placeholder'] = '';


    // if(ICL_LANGUAGE_CODE == 'vi'){
    //     $fields['billing']['billing_phone']['label'] = __('Số điện thoại');
    // } elseif (ICL_LANGUAGE_CODE == 'en') {
    //     $fields['billing']['billing_phone']['label'] = __('Phone');
    // }
    $fields['billing']['billing_phone']['label'] = __('Số điện thoại');
    $fields['billing']['billing_phone']['class'] = ['form-row-wide'];


    $fields['billing']['billing_email']['label'] = __('Email');
    $fields['billing']['billing_email']['class'] = ['form-row-wide'];


    //them field
    // if(ICL_LANGUAGE_CODE == 'vi'){
    //     $label_address = __('Địa chỉ');
    // } elseif (ICL_LANGUAGE_CODE == 'en') {
    //     $label_address = __('Address');
    // }
    // $fields['billing']['billing_address_3'] = array(
    //     'type'      => 'text',
    //     'label'     => $label_address,
    //     'class'     => array('form-row-wide'),
    //     'required'  => true
    // );


    // if(ICL_LANGUAGE_CODE == 'vi'){
    //     $fields['order']['order_comments']['label'] = __('Ghi chú');
    // } elseif (ICL_LANGUAGE_CODE == 'en') {
    //     $fields['order']['order_comments']['label'] = __('Note');
    // }
    $fields['order']['order_comments']['label'] = __('Ghi chú đơn hàng');
    $fields['order']['order_comments']['class'] = ['form-row-wide'];
    $fields['order']['order_comments']['placeholder'] = '';


    //them field
    // $fields['billing']['billing_message'] = array(
    //     'type'      => 'textarea',
    //     'label'     => __('Nội dung', 'woocommerce'),
    //     'placeholder'   => _x('', 'placeholder', 'woocommerce'),
    //     'class'     => array('form-row-wide'),
    //     'required'  => false,
    //     'clear'     => true,
    //     'label_class' => ''
    // );

    // echo '<pre>';
    // var_dump($fields);
    // echo '</pre>';
    
    return $fields;
}
add_filter('woocommerce_checkout_fields' , 'customCheckoutFields');


// Do bi ham nao do de len nen khong fix dc require nen anh duy lam the
// add_filter('woocommerce_get_country_locale_default', 'testfield');
// function testfield($data) {
//     $data['city']['label'] = 'Thanh pho';
//     $data['city']['required'] = false;
//     // echo '<pre>';
//     // var_dump($data);
//     // echo '</pre>';

//     return $data;
// }


// Xoá value mặc định của input form checkout, hoặc muốn điền sẵn thông tin đăng nhập của user vào form
add_filter( 'woocommerce_checkout_get_value', 'populating_checkout_fields', 10, 2 );
function populating_checkout_fields ( $value, $input ) {
    $token = ( ! empty( $_GET['token'] ) ) ? $_GET['token'] : '';
    // if( 'testtoken' == $token ) {
        // Define your checkout fields  values below in this array (keep the ones you need)
        $checkout_fields = array(
            'billing_first_name'    => ' ',
            // 'billing_last_name'     => 'Wick',
            // 'billing_company'       => 'Murders & co',
            // 'billing_country'       => 'US',
            'billing_address_1'     => ' ',
            // 'billing_address_2'     => 'Royal suite',
            // 'billing_address_3'          => ' ',
            // 'billing_city'          => 'Los Angeles',
            // 'billing_state'         => 'CA',
            // 'billing_postcode'      => '90102',
            'billing_phone'         => ' ',
            'billing_email'         => ' ',
            // 'shipping_first_name'   => 'John',
            // 'shipping_last_name'    => 'Wick',
            // 'shipping_company'      => 'Murders & co',
            // 'shipping_country'      => 'USA',
            // 'shipping_address_1'    => '7 Random street',
            // 'shipping_address_2'    => 'Royal suite',
            // 'shipping_city'         => 'Los Angeles',
            // 'shipping_state'        => 'California',
            // 'shipping_postcode'     => '90102',
            // 'account_password'       => '',
            'order_comments'        => ' ',
        );
        foreach( $checkout_fields as $key_field => $field_value ){
            if( $input == $key_field && ! empty( $field_value ) ){
                // $value = $field_value;
                $value = '';
            }
        }
    // }
    return $value;
}


// Sửa đơn vị giá mặc định của woocommerce
// add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);
// function change_existing_currency_symbol( $currency_symbol, $currency ) {
//     if($currency == 'VND') {
//         if(ICL_LANGUAGE_CODE == 'vi'){
//             $currency_symbol = 'đ';
//         } elseif (ICL_LANGUAGE_CODE == 'en') {
//             $currency_symbol = 'đ';
//         }
//     }
//     return $currency_symbol;
// }


// Thay đổi mũi tên trên breadcrumb của wc
// add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
// function jk_woocommerce_breadcrumbs() {
//     return array(
//             'delimiter'   => '<span class="wc-delimiter"> > </span>',
//             'wrap_before' => '<nav class="breadcrumbs wc-breadcrumbs" itemprop="breadcrumb">',
//             'wrap_after'  => '</nav>',
//             'before'      => '',
//             'after'       => '',
//             'home'        => _x( 'Trang chủ', 'breadcrumb', 'woocommerce' ),
//         );
// }


// Redirect đến trang cart khi add_to_cart thành công
// add_filter('woocommerce_add_to_cart_redirect', 'woocommerce_redirect_after_add_to_cart');
// function woocommerce_redirect_after_add_to_cart($url){
//     $url = get_data_language(get_page_link(34), get_page_link(11));
//     return $url;
// }
// Ko Redirect đến đâu cả khi add_to_cart thành công
// add_filter( 'woocommerce_add_to_cart_redirect', 'wp_get_referer' );


// Redirect trang thankyou khi order thành công (ko dung đc icl)
// add_action( 'template_redirect', 'woocommerce_redirect_after_checkout' );
// function woocommerce_redirect_after_checkout() {
//     global $wp;
//     if ( is_checkout() && ! empty( $wp->query_vars['order-received'] ) ) {

//         // $redirect_url = get_stylesheet_directory_uri().'/en/product/the-food-one';
//         $redirect_url = get_data_language(get_page_link(577), get_page_link(579));

//         wp_redirect($redirect_url );
//         exit;
//     }
// }


// Cũng là redirect trang thankyou, tại trang thankyou (ko dung đc icl)
// add_action( 'woocommerce_thankyou', 'bbloomer_redirectcustom');
// function bbloomer_redirectcustom( $order_id ){
//     $order = wc_get_order( $order_id );
//     $url = 'http://sangodanang.local/san-pham/san-pham-test';
//     if ( ! $order->has_status( 'failed' ) ) {
//         wp_safe_redirect( $url );
//         exit;
//     }
// }


// Thêm trường sắp xếp sản phẩm
// add_filter( 'woocommerce_get_catalog_ordering_args', 'custom_woocommerce_get_catalog_ordering_args' );
// function custom_woocommerce_get_catalog_ordering_args( $args ) {
//   $orderby_value = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
//     if ( 'random_list' == $orderby_value ) {
//         $args['orderby'] = 'rand';
//         $args['order'] = '';
//         $args['meta_key'] = '';
//     }
//     return $args;
// }
// add_filter( 'woocommerce_default_catalog_orderby_options', 'custom_woocommerce_catalog_orderby' );
// add_filter( 'woocommerce_catalog_orderby', 'custom_woocommerce_catalog_orderby' );
// function custom_woocommerce_catalog_orderby( $sortby ) {
//     $sortby['random_list'] = 'Random';
//     return $sortby;
// }

// Đổi tên trường sắp xếp sản phẩm
// add_filter( 'woocommerce_default_catalog_orderby_options', 'custom_woocommerce_catalog_orderby' );
// add_filter( 'woocommerce_catalog_orderby', 'custom_woocommerce_catalog_orderby' );
// function custom_woocommerce_catalog_orderby( $sortby ) {
//     $sortby['menu_order']   = 'Sắp sêp theo Tên';
//     $sortby['date']         = 'Sắp sêp theo Ngày';
//     $sortby['price']        = 'Sắp sêp theo Giá thấp';
//     $sortby['price-desc']   = 'Sắp sêp theo Giá cao';
//     $sortby['popularity']   = '';
//     $sortby['rating']   = '';
//     return $sortby;
// }





// Show số lượng sản phẩm đã được bán ra
// function show_count_price_product() {
//     global $post;
//     $count = get_post_meta($post->ID,'total_sales', true);
//     $result_count = sprintf( _n( '%s', '%s', $count, 'wpdocs_textdomain' ), number_format_i18n($count));
//     return $result_count;
// }


// Sửa validate email , ko cần
// add_action('woocommerce_checkout_process', 'bbloomer_matching_email_addresses');
// function bbloomer_matching_email_addresses() { 
//     $email1 = $_POST['billing_email'];
//     if (filter_var($email1, FILTER_VALIDATE_EMAIL)) {}else{
//         wc_add_notice( '<div class="wc_field_email">Địa chỉ Email của bạn không hợp lệ !</div>', 'error' );
//     }
// }


// Sửa nút mua hàng ajax (chưa cần)
// add_filter( 'woocommerce_loop_add_to_cart_link', 'chance_loop_add_to_cart_link' );
// function chance_loop_add_to_cart_link( $output ) {
//     global $product;
//     $test = sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
//         esc_url( $product->add_to_cart_url() ),
//         esc_attr( isset( $quantity ) ? $quantity : 1 ),
//         esc_attr( $product->get_id() ),
//         esc_attr( $product->get_sku() ),
//         esc_attr( isset( $class ) ? $class : 'button' ),
//         'Mua hàng'
//     );
//     return $test;
// }


// Sửa nút mua hàng page single product
// add_filter( 'woocommerce_product_single_add_to_cart_text', 'return_button_add_to_cart' );
// function return_button_add_to_cart( $output ) {
//     $output = 'Mua ngay';
//     return $output;
// }


// Thay đổi số lượng sp trên 1 trang (page shop)
// add_filter('loop_shop_columns', 'loop_columns', 999);
// if (!function_exists('loop_columns')) {
//     function loop_columns() {
//         $loop_columns = 4;
//         return $loop_columns;
//     }
// }

// add_filter( 'loop_shop_per_page', 'lw_loop_shop_per_page', 30 );
// function lw_loop_shop_per_page( $products ) {
//  $products = 2;
//  return $products;
// }

// update_option( 'woocommerce_catalog_columns', 4 );