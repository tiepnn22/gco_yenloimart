<?php
	/*
	Template Name: Mẫu Liên hệ
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //field
    $contact_contact_title      = get_field('contact_contact_title');
    $contact_contact_form_id    = get_field('contact_contact_form');
    $contact_contact_form       = do_shortcode('[contact-form-7 id="'.$contact_contact_form_id.'"]');

    $contact_map = get_field('contact_map');
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="pt-3">
    <div class="container">
        <h1 class="s24 text-center pb-4 contact-tit"><?php echo $page_name; ?></h1>
        
        <div class="maps">
            <?php echo $contact_map; ?>
        </div>

        <h2 class="s24 text-center pt-5 pb-4 contact-stit">
            <?php echo $contact_contact_title; ?>
        </h2>

        <?php if(!empty( $contact_contact_form )) { ?>
        <div class="pb-5 contact-frm">
            <?php echo $contact_contact_form; ?>
        </div>
        <?php } ?>
    </div>
</section>

<?php get_footer(); ?>