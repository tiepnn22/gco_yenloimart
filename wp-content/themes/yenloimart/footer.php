</main>

<?php
    //field
    $customer_slogan    = get_field('customer_slogan', 'option');
    $customer_address   = get_field('customer_address', 'option');
    $customer_phone     = get_field('customer_phone', 'option');
    $customer_phone_2   = get_field('customer_phone_2', 'option');
    $customer_email     = get_field('customer_email', 'option');

    $f_form_image   = get_field('f_form_image', 'option');
    $f_form_title   = get_field('f_form_title', 'option');
    $f_form_id      = get_field('f_form', 'option');
    $f_form         = do_shortcode('[contact-form-7 id="'.$f_form_id.'"]');

    $f_support_title    = get_field('f_support_title', 'option');
    $f_support_content  = get_field('f_support_content', 'option');

    $f_socical_title        = get_field('f_socical_title', 'option');
    $f_socical_facebook     = get_field('f_socical_facebook', 'option');
    $f_socical_googleplush  = get_field('f_socical_googleplush', 'option');
    $f_socical_twitter      = get_field('f_socical_twitter', 'option');
    $f_socical_skype        = get_field('f_socical_skype', 'option');

    $f_bottom_copyright = get_field('f_bottom_copyright', 'option');
?>

<footer class="ft">
    <section class="b1 regis mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img class="mt-40" src="<?php echo $f_form_image; ?>" title="" alt="">
                </div>
                <div class="col-md-8 d-flex align-items-center">
                    <div class="d-flex w-100 align-items-center flex-wrap">
                        <h2 class="s24 text-white"><?php echo $f_form_title; ?></h2>

                        <?php if(!empty( $f_form )) { ?>
                        <div class="ml-lg-5 ml-md-3 ml-0 regis-frm">
                            <?php echo $f_form; ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="b2 ft-1">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="text-lg-left text-center">
                        <!-- logo -->
                        <?php get_template_part("resources/views/logo-footer"); ?>
                    </div>
                    <div class="py-4 ft-about"><?php echo $customer_slogan; ?></div>
                    <ul class="list-unstyled s15 ft-add">
                        <li><i class="fas fa-map-marker-alt"></i> <?php echo $customer_address; ?></li>
                        <li><i class="fas fa-envelope"></i> 
                            Email: 
                            <a href="mailto:<?php echo $customer_email; ?>" title=""><?php echo $customer_email; ?></a>
                        </li>
                        <li><i class="fas fa-phone"></i> 
                            Hotline: 
                            <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>" title="">
                                <?php echo $customer_phone; ?>
                            </a> -
                            <a href="tel:<?php echo str_replace(' ','',$customer_phone_2);?>" title="">
                                <?php echo $customer_phone_2; ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h2 class="text-uppercase medium pb-4 medium ft-tit">
                        <?php echo wp_get_nav_menu_name("f-category" ); ?>
                    </h2>
                    <?php
                        if(function_exists('wp_nav_menu')){
                            $args = array(
                                'theme_location'    =>  'f-category',
                                'container'         =>  'nav',
                                'container_class'   =>  'nav-f-category',
                                'container_id'      =>  'nav-f-category',
                                'menu_class'        =>  'list-unstyled ft-list op8',
                            );
                            wp_nav_menu( $args );
                        }
                    ?>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h2 class="text-uppercase pb-4 medium ft-tit"><?php echo $f_support_title; ?></h2>
                    <div class="pb-2 ft-phone">
                        <?php echo $f_support_content; ?>
                    </div>
                    <h2 class="text-uppercase medium pt-4 pb-3 ft-tit"><?php echo $f_socical_title; ?></h2>
                    <ul class="list-unstyled social">
                        <li>
                            <a href="<?php echo $f_socical_facebook; ?>" title="" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $f_socical_googleplush; ?>" title="" target="_blank">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $f_socical_twitter; ?>" title="" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $f_socical_skype; ?>" title="" target="_blank">
                                <i class="fab fa-skype"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center b1 s15 light ft-last">
        <div class="container">
            <?php echo $f_bottom_copyright; ?>
        </div>
    </div>
</footer>

<!-- quickview -->
<div class="modal qv-modal" id="qv" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="ajaxLoad"></div>

</div>

<?php get_template_part("resources/views/socical-footer"); ?>

<?php wp_footer(); ?>
</body>
</html>