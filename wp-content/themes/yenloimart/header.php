<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="canonical" href="<?php echo get_page_link_current(); ?>" />
    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>


<script type="text/javascript">
    var ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
    // var path_dist = '<?php echo get_template_directory_uri(); ?>/dist/';
</script>


<?php
    //field
    $customer_address   = get_field('customer_address', 'option');
    $customer_phone     = get_field('customer_phone', 'option');
    $customer_phone_2   = get_field('customer_phone_2', 'option');
    $customer_work_time = get_field('customer_work_time', 'option');
?>

<div class="wrapper index">

<header class="fixed-top top">
    <div class="b1 top-wrapmenu">
        <div class="container">
            <div class="w-100 d-flex align-items-center justify-content-between top-menu">
                <div class="d-flex align-items-center justify-content-between w-100 top-menu-btn">
                    <!-- hamburger menu -->
                    <a id="nav-icon" href="#menu" class="d-xl-none">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                   
                    <!-- logo -->
                    <?php get_template_part("resources/views/logo"); ?>
                    
                    <!-- search -->
                    <?php get_template_part("resources/views/search-form"); ?>

                    <div class="menu-r">
                        <div class="d-flex align-items-center menu-r-top">
                            <i class="fas fa-search d-lg-none d-inline-block search-open"></i>
                            
                            <!-- cart -->
                            <?php get_template_part("resources/views/wc/wc-info-cart"); ?>

                            <div class="s14 text-lg-right top-contact">
                                <h2 class="top-add"><?php echo $customer_address; ?></h2>
                                <h3 class="bold s18">
                                    <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>" title="">
                                        <?php echo $customer_phone; ?>
                                    </a>
                                    <a href="tel:<?php echo str_replace(' ','',$customer_phone_2);?>" title="">
                                        <?php echo $customer_phone_2; ?>
                                    </a>
                                </h3>
                                <h4 class="top-time"><?php echo $customer_work_time; ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white">
        <div class="container">

            <!-- menu -->
            <?php get_template_part("resources/views/menu"); ?>

        </div>
    </div>
</header>

<main class="">