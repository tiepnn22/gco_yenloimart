<?php get_header(); ?>

<?php
	$page_id       = get_the_ID();
	$page_name     = get_the_title();
	$page_content  = get_the_content(); //woo phải dùng the_content()
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="pt-3">
    <div class="container">
        <h1 class="s24 text-center pb-4 contact-tit"><?php echo $page_name; ?></h1>
        
        <div class="<?php if( is_cart() || is_checkout() ) {} else { echo 'wp-editor-fix'; } ?>">
            <?php the_content(); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>