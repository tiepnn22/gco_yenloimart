<?php
	$post_id            = get_the_ID();
	$post_title 		= get_the_title($post_id);
	// $post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d/m/Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-post");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),100,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);

	$get_category = get_the_category($post_id);
?>

<div class="mb-4 blog-sitem">
    <div class="row">
        <div class="col-md-4 col-sm-5">
            <div class="text-center blog-simg">
                <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                	<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                </a>
            </div>
        </div>
        <div class="col-md-8 col-sm-7">
            <div class="blog-sinfo">
                <h3 class="bold s18 blog-info-tit">
                	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                		<?php echo $post_title; ?>
            		</a>
            	</h3>
                <h4 class="t6 s14 pb-2 blog-time "><?php echo $post_date; ?></h4>
                <div class="blog-info-wrap">
                    <p><?php echo $post_excerpt; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>