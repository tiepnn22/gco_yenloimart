<?php
	$post_id 			= get_the_ID();
	$post_title 		= get_the_title($post_id);
	// $post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d/m/Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-product");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),300,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);
?>

<div class="sale-item">
    <div class="sale-item-top">
        <div class="text-center">
            <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
            	<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
            </a>
        </div>
        <div class="text-center sale-item-content">
            <h3 class="medium s18 sale-item-content-tit">
            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
            		<?php echo $post_title; ?>
        		</a>
        	</h3>
            <h4 class="s16 t2 medium sale-item-price">
                <?php echo show_price_old_price($post_id); ?>
            </h4>
        </div>
    </div>
    <ul class="list-unstyled pact">
        <li>
            <?php echo show_add_to_cart_button_ajax($post_id); ?>
        </li>
        <li>
            <a href="javascript:void(0)" class="btn detail-btn hover-product"  data-productid="<?php echo $post_id; ?>">
                Chi tiết
            </a>
        </li>
    </ul>
</div>