<?php
	$post_id            = get_the_ID();
	$post_title 		= get_the_title($post_id);
	// $post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d/m/Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-post");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),100,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);

	$get_category = get_the_category($post_id);
?>

<div class="col-lg-4 col-md-6 col-sm-6">
    <article class="blog-item">
        <figure class="blog-img text-center">
            <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" class="link-ef">
                <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
            </a>
        </figure>
        <figcaption class="blog-item-info">
            <h3 class="bold s18 blog-info-tit">
                <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                    <?php echo $post_title; ?>
                </a>
            </h3>
            <h4 class="t6 s14 pb-2 blog-time "><?php echo $post_date; ?></h4>
            <div class="blog-info-wrap">
                <p><?php echo $post_excerpt; ?></p>
            </div>
        </figcaption>
    </article>
</div>