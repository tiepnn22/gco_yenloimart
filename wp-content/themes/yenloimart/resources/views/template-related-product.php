<?php
	global $post;
	$terms 		= get_the_terms( $post->ID , 'product_cat', 'string');
	$term_ids 	= wp_list_pluck($terms,'term_id');
	
	$query = new WP_Query( array(
		'post_type' 	 => 'product',
		'tax_query' 	 => array(
			array(
				'taxonomy' 	=> 'product_cat',
				'field' 	=> 'id',
				'terms' 	=> $term_ids,
				'operator'	=> 'IN'
			 )),
		'posts_per_page' => -1,
		'orderby' 		 => 'date',
		'post__not_in'	 => array($post->ID)
	) );
?>


<section class="py-5 pdetail-re">
    <div class="container">
        <h2 class="s24 text-center pb-4 pdetail-info-tit">Sản phẩm liên quan</h2>
        <div class="owl-carousel pdetail-reslider">

			<?php
				if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
			?>

				<?php get_template_part('resources/views/content/related-product', get_post_format()); ?>

			<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

        </div>
    </div>
</section>