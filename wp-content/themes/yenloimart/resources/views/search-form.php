<div class="px-lg-4 search-dropdown">

	<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="trans d-flex align-items-center search-frm">
	    <input type="text" class="form-control light s14 search-ip" required="required" placeholder="<?php _e('Nhập từ khóa...', 'text_domain'); ?>" name="s" value="<?php echo get_search_query(); ?>">
	    <button type="submit" class="btn search-btn">
	    	<img src="<?php echo asset('images/search.png'); ?>" alt="">
	    </button>
	</form>
	
</div>