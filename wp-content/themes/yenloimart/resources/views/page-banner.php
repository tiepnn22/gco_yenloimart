<?php
    //field
	$image_link_check = get_field('page_banner_default', 'option');
	$image_link = (!empty($image_link_check)) ? $image_link_check : asset('images/21.jpg');
?>

<section class="banner-img">
    <div class="container-flush">
        <a href="javascript:void(0)" title="" class="link-ef">
        	<img src="<?php echo $image_link; ?>" alt="">
        </a>
    </div>
</section>