<!--fb comments-->
<div class="fb-comments" data-href='<?php the_permalink();?>' data-width="100%" data-numposts="5" data-colorscheme="light"></div>

<?php
    //check language for fb
    switch (ICL_LANGUAGE_CODE) {
        case 'en':
            $lang = 'en_US';
            break;
        case 'ko':
            $lang = 'ko_KR';
            break;
        default:
            $lang = 'vi_VN';
            break;
    }
?>
<!--sdk fb comments, like, share-->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/<?php echo $lang; ?>/sdk.js#xfbml=1&version=v9.0" nonce="mZ55CjvT"></script>