<?php
    // get link page contact
    $link_page_contact = get_link_page_template('template-contact.php');

    // get id page home
    $id_page_home = get_option('page_on_front');

    //field
    $h_logo = get_field('h_logo', 'option');

    $home_ads_image     = get_field('home_ads_image', $id_page_home);
    $home_ads_content   = get_field('home_ads_content', $id_page_home);
?>

<section class="scontact" style="background: url(<?php echo $home_ads_image; ?>); background-size: cover;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="text-center scontact-info">
                    <div class="text-center">
                    	<img src="<?php echo $h_logo; ?>" title="" alt="">
                    </div>
                    <h2 class="t2 text-center text-uppercase medium s18 pt-3 pb-4 scontact-info-tit">
                    	<?php echo $home_ads_content; ?>
                    </h2>
                    <div class="text-center">
                        <a href="<?php echo $link_page_contact; ?>" title="" class="btn text-uppercase scontact-btn">Liên hệ ngay</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>