<?php get_header(); ?>

<h1 style="display: none;"><?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?></h1>

<?php
	//field
	$h_logo = get_field('h_logo', 'option');
    $customer_slogan    = get_field('customer_slogan', 'option');
    $customer_address   = get_field('customer_address', 'option');
    $customer_phone     = get_field('customer_phone', 'option');
    $customer_phone_2   = get_field('customer_phone_2', 'option');
    $customer_email     = get_field('customer_email', 'option');

	$home_slide = get_field('home_slide');

	$home_product_title	= get_field('home_product_title');
	$home_product_desc 	= get_field('home_product_desc');

	$home_service_image_bg	= get_field('home_service_image_bg');
	$home_service_image		= get_field('home_service_image');
	$home_service_title		= get_field('home_service_title');
	$home_service_desc		= get_field('home_service_desc');
	$home_service_content	= get_field('home_service_content');

	// $home_ads_image		= get_field('home_ads_image');
	// $home_ads_content	= get_field('home_ads_content');

	$home_testimonial_title		= get_field('home_testimonial_title');
	$home_testimonial_desc		= get_field('home_testimonial_desc');
	$home_testimonial_content	= get_field('home_testimonial_content');

	$home_contact_image_bg		= get_field('home_contact_image_bg');
	$home_contact_image		= get_field('home_contact_image');
	$home_contact_desc		= get_field('home_contact_desc');
	$home_contact_form_id	= get_field('home_contact_form');
    $home_contact_form		= do_shortcode('[contact-form-7 id="'.$home_contact_form_id.'"]');

	$home_partner_title		= get_field('home_partner_title');
	$home_partner_content	= get_field('home_partner_content');

    $terms_info = get_terms( 'product_cat', array(
        // 'parent'       => 0,
        'hide_empty'   => true
    ) );
?>

<?php if(!empty( $home_slide )) { ?>
<section class="slider-area">
    <div class="slider-l">
        <div id="slider">

			<?php
			    foreach ($home_slide as $foreach_kq) {

			    $post_image = $foreach_kq["image"];
			    $post_url 	= $foreach_kq["url"];
			?>
		        <a href="<?php echo $post_url; ?>" title="">
		        	<img class="slider-img" src="<?php echo $post_image; ?>" alt="slider-img" title="" />
		        </a>
			<?php } ?>

        </div>
    </div>
</section>
<?php } ?>

<section class="mt-90 cate">
    <div class="container">
        <div class="owl-carousel cate-slider">

            <?php
                foreach ( $terms_info as $foreach_kq ) {
                    $term_id		= $foreach_kq->term_id;
                    $taxonomy_slug	= $foreach_kq->taxonomy;
                    $term_desc		= cut_string( $foreach_kq->description ,300,'...');
                    $term_name		= get_term( $term_id, $taxonomy_slug )->name;
                    $term_link		= get_term_link(get_term( $term_id ));
                    $thumbnail_id       = get_term_meta( $term_id, 'thumbnail_id', true );
                    $term_image_check   = wp_get_attachment_url( $thumbnail_id ); // woo
                    $term_image         = (!empty($term_image_check)) ? $term_image_check : asset('images/no-image-wc.png');
                ?>
                    <article class="text-white cate-slider-item">
                        <figure class="text-center cate-slider-img">
                            <a class="link-ef" href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
                                <img src="<?php echo $term_image; ?>" alt="<?php echo $term_name; ?>">
                            </a>
                        </figure>
                        <figcaption class="cate-slider-info">
                            <div class="s15 cate-slider-info-wrap">
                                <h2 class="s24 medium cate-slider-tit">
                                    <a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
                                        <?php echo $term_name; ?>
                                    </a>
                                </h2>
                                <p><?php echo $term_desc; ?></p>
                            </div>
                            <div class="text-lg-left text-center">
                                <a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>" class="s12 btn more-btn">
                                    Xem chi tiết
                                </a>
                            </div>
                        </figcaption>
                    </article>
                <?php
                }
            ?>

        </div>
    </div>
</section>

<section class="green-pro">
    <div class="container-fluid">
        <h1 class="f1 s40 text-center t1 green-pro-tit"><?php echo $home_product_title; ?></h1>
        <div class="w-lg-70 text-center pt-2 pb-3">
            <h2 class=""><?php echo $home_product_desc; ?></h2>
        </div>

        <ul class="nav nav-pills justify-content-center justify-content-lg-center wel-tabs" role="tablist">
            <li class="nav-item">
                <a class="active" data-toggle="pill" href="#sp_0">Tất cả</a>
            </li>

            <?php
                $i=1;
                foreach ( $terms_info as $foreach_kq ) {
					$term_id		= $foreach_kq->term_id;
					$taxonomy_slug	= $foreach_kq->taxonomy;
					// $term_desc		= cut_string( $foreach_kq->description ,300,'...');
					$term_name		= get_term( $term_id, $taxonomy_slug )->name;
					// $term_link		= get_term_link(get_term( $term_id ));
					// $thumbnail_id 		= get_term_meta( $term_id, 'thumbnail_id', true );
					// $term_image_check	= wp_get_attachment_url( $thumbnail_id ); // woo
					// $term_image			= (!empty($term_image_check)) ? $term_image_check : asset('images/no-image-wc.png');
            ?>
		            <li class="nav-item">
		                <a data-toggle="pill" href="#sp_<?php echo $i; ?>"><?php echo $term_name; ?></a>
		            </li>
            <?php
                $i++; }
            ?>
        </ul>

        <div class="tab-content">

            <div class="tab-pane fade show active" id="sp_0">
                <div class="row justify-content-between ptab">

		            <?php
		    			$query = query_post_by_custompost('product', 12);
		                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
		            ?>

		                <?php get_template_part('resources/views/content/home-product-tab', get_post_format()); ?>

		            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                </div>
            </div>

            <?php
                $i=1;
                foreach ( $terms_info as $foreach_kq ) {
					$term_id		= $foreach_kq->term_id;
					$taxonomy_slug	= $foreach_kq->taxonomy;
					// $term_desc		= cut_string( $foreach_kq->description ,300,'...');
					// $term_name		= get_term( $term_id, $taxonomy_slug )->name;
					// $term_link		= get_term_link(get_term( $term_id ));
					// $thumbnail_id 		= get_term_meta( $term_id, 'thumbnail_id', true );
					// $term_image_check	= wp_get_attachment_url( $thumbnail_id ); // woo
					// $term_image			= (!empty($term_image_check)) ? $term_image_check : asset('images/no-image-wc.png');
            ?>
		            <div class="tab-pane fade" id="sp_<?php echo $i; ?>">
		                <div class="row justify-content-between ptab">

				            <?php
				    			$query = query_post_by_taxonomy('product', $taxonomy_slug, $term_id, 12);
				                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
				            ?>

				                <?php get_template_part('resources/views/content/home-product-tab', get_post_format()); ?>

				            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

		                </div>
		            </div>
            <?php
                $i++; }
            ?>
        </div>
    </div>
</section>

<section class="about">
    <div class="container">
        <h2 class="f1 s40 text-center t1 about-tit"><?php echo $home_service_title; ?></h2>
        <div class="s15 w-lg-70 text-center pt-3 pb-4">
            <h2 class=""><?php echo $home_service_desc; ?></h2>
        </div>
        <div class="about-wrap" style="background: url(<?php echo $home_service_image_bg; ?>) no-repeat center center;">
            <div class="row justify-content-between about-row">

				<?php if(!empty( $home_service_content )) { ?>
				<?php
				    foreach ($home_service_content as $foreach_kq) {

				    $post_title = $foreach_kq["title"];
				    $post_desc  = $foreach_kq["desc"];
				?>
	                <div class="col-lg-6 col-md-5 wow fadeInLeft" data-wow-offset="150">
	                    <div class="about-item">
	                        <h3 class="t1 medium s18 about-item-tit"><?php echo $post_title; ?></h3>
	                        <div class="t3 s15 about-item-wrap">
	                            <h4><?php echo $post_desc; ?></h4>
	                        </div>
	                    </div>
	                </div>
				<?php } ?>
				<?php } ?>

            </div>
            <div class="text-center about-img wow fadeInUp" data-wow-offset="150">
                <img src="<?php echo $home_service_image; ?>" title="" alt="">
            </div>
        </div>
    </div>
</section>

<?php get_template_part("resources/views/home-ads"); ?>

<section class="cus">
    <div class="container">
        <h2 class="f1 s40 text-center t1 green-pro-tit"><?php echo $home_testimonial_title; ?></h2>
        <div class="w-lg-70 text-center pt-3 pb-4">
            <h2 class=""><?php echo $home_testimonial_desc; ?></h2>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="owl-carousel cus-slider">

					<?php if(!empty( $home_testimonial_content )) { ?>
					<?php
					    foreach ($home_testimonial_content as $foreach_kq) {

					    $post_image = $foreach_kq["image"];
					    $post_title = $foreach_kq["title"];
					    $post_job   = $foreach_kq["job"];
					    $post_desc  = $foreach_kq["desc"];
					?>
	                    <div class="text-center item">
	                        <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
	                        <div class="cus-content">
	                            <h3 class="medium s18 text-capitalize"><?php echo $post_title; ?></h3>
	                            <h4 class="t2 s15 cus-job"><?php echo $post_job; ?></h4>
	                            <p><?php echo $post_desc; ?></p>
	                        </div>
	                    </div>
					<?php } ?>
					<?php } ?>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="b1 mcontact" style="background: url(<?php echo $home_contact_image_bg; ?>) no-repeat center top; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
                <div class="text-center">

                    <img src="<?php echo $h_logo; ?>" alt="">
                    <ul class="text-white list-unstyled mcontact-list">
                        <li><i class="fas fa-map-marker-alt"></i></li>
                        <li><?php echo $customer_address; ?></li>

                        <li><i class="fas fa-at"></i></li>
                        <li><a href="mailto:<?php echo $customer_email; ?>" title=""><?php echo $customer_email; ?></a></li>

                        <li><i class="fas fa-phone"></i></li>
                        <li>
                        	<a href="tel:<?php echo str_replace(' ','',$customer_phone);?>" title="">
                        		<?php echo $customer_phone; ?>
                        	</a> - 
                        	<a href="tel:<?php echo str_replace(' ','',$customer_phone_2);?>" title="">
                        		<?php echo $customer_phone_2; ?>
                    		</a>
                    	</li>
                    </ul>

                </div>
            </div>
            <div class="col-lg-9 col-md-8">
                <div class="ml-auto contact-frm">
                    <div class="row justify-content-end">
                        <div class="col-lg-4">
                            <img src="<?php echo $home_contact_image; ?>" alt="">
                        </div>
                        <div class="col-lg-8">
                            <h2 class="contact-frm-tit"><?php echo $home_contact_desc; ?></h2>

					        <?php if(!empty( $home_contact_form )) { ?>
					            <?php echo $home_contact_form; ?>
					        <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="brand">
    <div class="container">
        <h2 class="f1 s40 text-center t1 green-pro-tit"><?php echo $home_partner_title; ?></h2>
        <div class="owl-carousel brand-slider">

			<?php if(!empty( $home_partner_content )) { ?>
			<?php
			    foreach ($home_partner_content as $foreach_kq) {

			    $post_image = $foreach_kq;
			?>
	            <div class="text-center brand-item">
	                <img src="<?php echo $post_image; ?>" alt="">
	            </div>
			<?php } ?>
			<?php } ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>

