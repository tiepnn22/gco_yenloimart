<?php
	/*
	Template Name: Mẫu Tin tức
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    $sticky = get_option( 'sticky_posts' );
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="green-pro">
    <div class="container">
        <h1 class="s24 pb-4 text-center blog-tit"><?php echo $page_name; ?></h1>

        <div class="row justify-content-between">

            <?php
                $query_sticky =  new WP_Query( array(
                        'post_type'     => 'post',
                        'post__in'      => get_option( 'sticky_posts' ),
                        'showposts'     => 3,
                        'order'         => 'DESC',
                        'orderby'       => 'date'
                ) );
                if($query_sticky->have_posts()) : while ($query_sticky->have_posts() ) : $query_sticky->the_post();
            ?>

                <?php get_template_part('resources/views/content/category-sticky-post', get_post_format()); ?>

            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

            <div class="col-12">
                <div class="sblog">
                    <div class="row">
                        <div class="col-lg-9 col-md-8">
                            <div class="sblog-wrap">

                                <?php
                                    $query =  new WP_Query( array(
                                            'post_type'     => 'post',
                                            'post__not_in'  => get_option( 'sticky_posts' ),
                                            'showposts'     => 4,
                                            'order'         => 'DESC',
                                            'orderby'       => 'date',
                                            'paged'         => (get_query_var('paged')) ? get_query_var('paged') : 1
                                    ) );
                                    $max_num_pages = $query->max_num_pages;

                                    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                                ?>

                                    <?php get_template_part('resources/views/content/category-post', get_post_format()); ?>

                                <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                            </div>

                            <!--pagination-->
                            <?php echo paginationCustom( $max_num_pages ); ?>
                        </div>

                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>