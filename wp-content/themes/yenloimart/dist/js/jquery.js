$(document).ready(function($){

  // Ajax Readmore_post Product
  jQuery(".hover-product").click(function(e) {
      var data_productid = jQuery(this).data('productid');
      console.log(data_productid);

      jQuery.ajax({
          type: "POST",
          url: ajax_url,
          data: {
              action: 'Readmore_post',
              data_productid : data_productid
          },
          beforeSend:function(){
              $('.ajaxLoad').show();
          },
          success:function(response){
              var data = jQuery.parseJSON(response);
              jQuery(".modal-content").html(data.result);
              $('.ajaxLoad').hide();

              $('.modal.qv-modal').addClass('d-block');
              jQuery(".modal-content button.close").click(function(e) {
                  $('.modal.qv-modal').removeClass('d-block');
              });
              // data_slider();
          }
      });
  });
  
  // Ajax Delete_product_cart
  jQuery(".single-cart-item .trash").click(function(e) {
      var data_productid = jQuery(this).data('productid');
      console.log(data_productid);

      jQuery.ajax({
          type: "POST",
          url: ajax_url,
          data: {
              action: 'Delete_product_cart',
              data_productid : data_productid
          },
          success:function(response){
              var data = jQuery.parseJSON(response);
              
              if(data.result == 2) {
                  location.reload();
              }
          }
      });
  });

  // fix main padding-top to header
  var header_height = $('header').height();
  $('main').css('padding-top', header_height);

  if($('header.top').length){
    $(window).scroll(function(){
      /*var anchor = $('header.top').offset().top;*/
      var anchor = $('header.top').offset().top;
      /*console.log(anchor);*/
      if(anchor >= 130){
          $('header.top').addClass('cmenu');
          $('.cate-list').removeClass('on');
      }
      else{
          $('header.top').removeClass('cmenu');
      }
    });
  }

  // new WOW().init();

  // if($('.to-top').length){
  //   $('.to-top').on('click',function(event){
  //       event.preventDefault();
  //   $('body, html').stop().animate({scrollTop:0},800)});
  //   $(window).scroll(function(){
  //       var anchor=$('.to-top').offset().top;
  //       if(anchor>1000){
  //           $('.to-top').css('opacity','1')
  //       }
  //       else{
  //           $('.to-top').css('opacity','0')
  //       }
  //   });
  // }

  $("#menu").mmenu({
    "extensions": [
          "pagedim-black",
          "shadow-panels"
       ]
      // options
      /*"offCanvas": {
              "position": "right"
          }*/
    }, {
        // configuration
        clone: true
  });

  //Tooltip
  $('[data-toggle="tooltip"]').tooltip();

   /* nivoSlider */ 
  $("#slider").nivoSlider({ 
    effect: 'random',                 // Specify sets like: 'fold,fade,sliceDown' 
    slices: 15,                       // For slice animations 
    boxCols: 8,                       // For box animations 
    boxRows: 4,                       // For box animations 
    animSpeed: 1300,                   // Slide transition speed 
    pauseTime: 5000,                  // How long each slide will show 
    startSlide: 0,                    // Set starting Slide (0 index) 
    directionNav: false,               // Next & Prev navigation 
    controlNav: false,                 // 1,2,3... navigation 
    controlNavThumbs: false,          // Use thumbnails for Control Nav 
    pauseOnHover: true,               // Stop animation while hovering 
    manualAdvance: false,             // Force manual transitions 
    randomStart: true,               // Start on a random slide 
    beforeChange: function(){},       // Triggers before a slide transition 
    afterChange: function(){},        // Triggers after a slide transition 
    slideshowEnd: function(){},       // Triggers after all slides have been shown 
    lastSlide: function(){},          // Triggers when last slide is shown 
    afterLoad: function(){}           // Triggers when slider has loaded 
  });

  $('.owl-carousel.cate-slider').owlCarousel({
    items: 4,
    margin: 0,
    autoplay: false,
    autoplayHoverPause: true,
    autoplayTimeout: 2000,
    smartSpeed: 400,
    rewind: true,
    /*navText: ['<i class="fas fa-long-arrow-alt-right"></i>','<i class="fas fa-long-arrow-alt-left"></i>'],*/
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        480:{
            items:2
        },
        768:{
            items:2
        },
        992:{
            items:4
        },
        1200:{
            items:4
        }
    }
  });
  $('.owl-carousel.pdetail-reslider').owlCarousel({
    items: 4,
    margin: 20,
    autoplay: false,
    autoplayHoverPause: true,
    autoplayTimeout: 2000,
    smartSpeed: 400,
    rewind: true,
    /*navText: ['<i class="fas fa-long-arrow-alt-right"></i>','<i class="fas fa-long-arrow-alt-left"></i>'],*/
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        768:{
            items:2
        }
        ,
        1200:{
            items:4
        }
    }
  });
  $('.owl-carousel.cus-slider').owlCarousel({
    autoplay: true,
    items: 1,
    dots: true,
    autoplayHoverPause: true,
    autoplayTimeout: 3500,
    smartSpeed: 400,
    rewind: true,
    /*navText: ['<i class="fas fa-long-arrow-alt-right"></i>','<i class="fas fa-long-arrow-alt-left"></i>'],*/
  });

  $('.owl-carousel.brand-slider').owlCarousel({
    items: 4,
    margin: 20,
    autoplay: true,
    autoplayHoverPause: true,
    autoplayTimeout: 2000,
    smartSpeed: 400,
    rewind: true,
    /*navText: ['<i class="fas fa-long-arrow-alt-right"></i>','<i class="fas fa-long-arrow-alt-left"></i>'],*/
    responsiveClass:true,
    responsive:{
        0:{
            items:2
        },
        992:{
            items:3
        }
        ,
        1200:{
            items:4
        }
    }
  });
  $('.bdetail-reslider').owlCarousel({
    items: 2,
    margin: 20,
    autoplay: true,
    autoplayHoverPause: true,
    autoplayTimeout: 2000,
    smartSpeed: 400,
    rewind: true,
    /*navText: ['<i class="fas fa-long-arrow-alt-right"></i>','<i class="fas fa-long-arrow-alt-left"></i>'],*/
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        768:{
            items:2
        }
    }
  });

  $('.search-open').on('click', function(event) {
    event.preventDefault();
    $('.search-frm').toggleClass('on');
  });

  // $(".button").on("click", function() {
  //   var $button = $(this);
  //   var oldValue = $button.parent().find("input").val();
  //   if ($button.text() == "+") {
  //     var newVal = parseFloat(oldValue) + 1;
  //   } else {
  //     // Don't allow decrementing below zero
  //     if (oldValue > 0) {
  //       var newVal = parseFloat(oldValue) - 1;
  //     } else {
  //       newVal = 0;
  //     }
  //   }
  //   $button.parent().find("input").val(newVal);
  // });
  
  $('.nav-link .custom-control.custom-radio').on('click', function(e){
    e.preventDefault();
    /*var a = $(this).children('.custom-control-input').prop('value');*/
    $(this).children('.custom-control-input').prop('checked',true);
  });

  if($("[data-fancybox]").length){
    $("[data-fancybox]").fancybox({});
    if($('.linkyoutube').length) {
      var url = $('.linkyoutube').attr('href').replace('watch?v=', 'embed/');
      $('.linkyoutube').attr('href', url);
    }
    
  }

  /*slider range*/
  if($("#range").length) {
    $("#range").ionRangeSlider({
      hide_min_max: true,
      keyboard: true,
      min: 1000,
      max: 10000000,
      from: 400000,
      to: 7000000,
      type: 'double',
      step: 1000,
      prefix: "",
      postfix: " vnđ",
      grid: true
    });
  }


  $('.paside-chkbox li').on('click', function(event) {
      event.preventDefault();
      $(this).toggleClass('active').children('.custom-control-input').prop('checked', !($(this).children('.custom-control-input').is(':checked')));
  });
  $('.paside-list li.active').children('.custom-control-input').prop('checked', 'true');

  $('.paside-radio li').on('click', function(event) {
      event.preventDefault();
      $('.paside-radio li').removeClass('active');
      $(this).toggleClass('active').children('.custom-control-input').prop('checked', true);
  });
  /*$('.paside-list li.active').children('.custom-control-input').prop('checked', 'true');*/

  $('.top-cate h2').on('click', function(event) {
    event.preventDefault();
    $(this).next('.top-cate-list').slideToggle();
  });
  $('.search-open').on('click', function(event) {
    event.preventDefault();
    $('.search-dropdown').toggleClass('on');
  });
  $('.pdetail-seemore').on('click', function(event) {
    event.preventDefault();
    console.log('click');
    $(this).parent('.pdetail-wrap').toggleClass('open');
  });

  /* choice gender */
  $('.pdetail-gen label.active').children('input').prop('checked', 'true');
  $('.pdetail-gen label').on('click', function(event) {
    /*event.preventDefault();*/
    $('.pdetail-gen label').removeClass('active');
    $(this).addClass('active');
  });

  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
  });
});
/*
http://jsfiddle.net/LCB5W/
https://stackoverflow.com/questions/152975/how-do-i-detect-a-click-outside-an-element

https://codepen.io/altro-nvp2/pen/MmQBVd
http://www.landmarkmlp.com/js-plugin/owl.carousel/demos/transitions.html
https://codepen.io/radimby/pen/YpEJQP
*/