<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' ); ?>

<?php
    $term_info          = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $term_id            = $term_info->term_id;
    $term_name_check    = $term_info->name;
    $term_name          = (!empty($term_name_check)) ? $term_name_check : 'Sản phẩm';
    $term_desc          = cut_string( $term_info->description ,300,'...');
    $term_link          = esc_url(get_term_link($term_id));
    $taxonomy_slug      = $term_info->taxonomy;
    $thumbnail_id       = get_term_meta( $term_id, 'thumbnail_id', true );
    $term_image_check   = wp_get_attachment_url( $thumbnail_id ); // woo
    $term_image         = (!empty($term_image_check)) ? $term_image_check : ''; // woo

    //field
    $image_link_check = get_field('page_banner_product', 'option');
    $image_link = (!empty($image_link_check)) ? $image_link_check : asset('images/bapbap.png');

    $terms_info = get_terms( 'product_cat', array(
        // 'parent'       => 0,
        'hide_empty'   => true
    ) );
?>

<section class="banner" style="background-image: linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .3)), url(<?php echo $image_link; ?>);">
    <div class="container">
        <div class="banner-wrap">
            <div class="text-center">
                <img src="<?php echo asset('images/bapbap.png'); ?>" alt="">
            </div>
            <h1 class="t4 s30 text-center text-uppercase bold py-4"><?php echo $term_name; ?></h1>
        </div>
    </div>
</section>

<section class="green-pro">
    <div class="container">

        <?php
            if ( woocommerce_product_loop() ) {
        ?>

            <!--order-->
            <?php
                // do_action( 'woocommerce_before_shop_loop' );
            ?>

            <div class="">
                <div class="row justify-content-between ptab">

                    <?php if(!empty( $term_image )) { ?>
                    <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                        <a class="link-ef" href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
                            <img src="<?php echo $term_image; ?>" alt="<?php echo $term_name; ?>">
                        </a>
                    </div>
                    <?php } ?>

                    <?php
                        // woocommerce_product_loop_start();
                        if ( wc_get_loop_prop( 'total' ) ) {
                            while ( have_posts() ) {
                                the_post();

                                // do_action( 'woocommerce_shop_loop' );
                                // wc_get_template_part( 'content', 'product' );
                                get_template_part('resources/views/content/category-product', get_post_format());
                            }
                        }
                        // woocommerce_product_loop_end();
                    ?>
                </div>

                <!--pagination-->
                <?php do_action( 'woocommerce_after_shop_loop' ); ?>
            </div>

        <?php
                // do_action( 'woocommerce_after_shop_loop' );
            } else {
                do_action( 'woocommerce_no_products_found' );
            }
        ?>

    </div>
</section>

<section class="mb-5 cate">
    <div class="container">
        <div class="owl-carousel cate-slider">

            <?php
                foreach ( $terms_info as $foreach_kq ) {
                    $term_id        = $foreach_kq->term_id;
                    $taxonomy_slug  = $foreach_kq->taxonomy;
                    $term_desc      = cut_string( $foreach_kq->description ,300,'...');
                    $term_name      = get_term( $term_id, $taxonomy_slug )->name;
                    $term_link      = get_term_link(get_term( $term_id ));
                    $thumbnail_id       = get_term_meta( $term_id, 'thumbnail_id', true );
                    $term_image_check   = wp_get_attachment_url( $thumbnail_id ); // woo
                    $term_image         = (!empty($term_image_check)) ? $term_image_check : asset('images/no-image-wc.png');
                ?>
                    <article class="text-white cate-slider-item">
                        <figure class="text-center cate-slider-img">
                            <a class="link-ef" href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
                                <img src="<?php echo $term_image; ?>" alt="<?php echo $term_name; ?>">
                            </a>
                        </figure>
                        <figcaption class="cate-slider-info">
                            <div class="s15 cate-slider-info-wrap">
                                <h2 class="s24 medium cate-slider-tit">
                                    <a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
                                        <?php echo $term_name; ?>
                                    </a>
                                </h2>
                                <p><?php echo $term_desc; ?></p>
                            </div>
                            <div class="text-lg-left text-center">
                                <a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>" class="s12 btn more-btn">
                                    Xem chi tiết
                                </a>
                            </div>
                        </figcaption>
                    </article>
                <?php
                }
            ?>

        </div>
    </div>
</section>

<?php

get_footer( 'shop' );
