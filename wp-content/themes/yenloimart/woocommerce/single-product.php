<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<?php
    $product_id = get_the_ID();

	$terms = wp_get_object_terms($post->ID, 'product_cat');
	if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];
    $term_id        = $term->term_id;
    $term_name      = $term->name;
    // $term_desc      = cut_string( $term->description ,300,'...');
    // $term_link      = esc_url(get_term_link($term_id));
    // $taxonomy_slug  = $term->taxonomy;
    // $thumbnail_id        = get_term_meta( $term_id, 'thumbnail_id', true ); // woo
    // $term_image_check    = wp_get_attachment_url( $thumbnail_id ); // woo
    // $term_image          = (!empty($term_image_check)) ? $term_image_check : ''; // woo

    // woocommerce
    $product = new WC_product($product_id);

    	// price
    	$product_info    = wc_get_product( $product_id );
    	$old_price  = (float)$product_info->get_regular_price();

        // tag
        // $single_product_tag = $product->get_tags();

        // gallery
        // $single_product_gallery = $product->get_gallery_image_ids();

        // check stock
        if ( ! $product->managing_stock() && ! $product->is_in_stock() ) {
            $product_stock = 'Hết hàng';
        } else {
            $product_stock = 'Còn hàng';
        }

    	// check stock quantity
    	$product_stock_quantity_check = $product->get_stock_quantity();
    	if($product_stock_quantity_check > 0){
    		$product_stock_quantity = 'Còn '.$product_stock_quantity_check.' sản phẩm';
    	} else {
    		$product_stock_quantity = 'Hết hàng';
    	}

        // info sku
        // $product_sku = $product->get_sku();

        // info attribute
        // $array = wc_get_attribute_taxonomies();
        // foreach ($array as $foreach_kq) {
        //     $attribute_id = $foreach_kq->attribute_id;
        //     $attribute_name = $foreach_kq->attribute_name;
        // }
        // $product_brand = $product->get_attribute( $attribute_name );
    
    // info product
    $single_product_title       = get_the_title($product_id);
    $single_product_date        = get_the_date('d/m/Y', $product_id);
    $single_product_link        = get_permalink($product_id);
    $single_product_image       = getPostImage($product_id,"full");
    $single_product_excerpt     = get_the_excerpt($product_id);
    $single_recent_author       = get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
    $single_product_author      = $single_recent_author->display_name;
    // $single_product_tag         = get_the_tags($product_id);
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="green-pro">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="pdetail-l">
                    <a href="<?php echo $single_product_image; ?>" class="MagicZoom" data-options="zoomPosition: inner">
                    	<img src="<?php echo $single_product_image; ?>">
                    </a>
                </div>
            </div>

            <div class="col-md-6">
                <div class="pdetail-r">
                    <h1 class="s24 text-capitalize pdetail-tit"><?php echo $single_product_title; ?></h1>
                    <div class="medium d-flex flex-wrap pdetail-price">
                    	<?php echo show_price_old_price($product_id); ?>
                    </div>

                    <h3 class="medium s18 pdetail-stit">Thông tin</h3>
                    <ul class="">
                        <?php echo $single_product_excerpt; ?>

                        <li>
                        	<span>Tình trạng:</span> 
                        	<span class="pdetail-stt">
                        		<?php
                        			if(!isset($product_stock_quantity_check)){
                        				echo $product_stock;
                        			} else {
                    					echo $product_stock_quantity;
                        			}
                        		?>
                        	</span>
                        </li>
                    </ul>

                    <?php if($old_price > 0){ ?>
                    <h3 class="medium s18 pdetail-stit">Số lượng</h3>
                	<?php } ?>
                    <div class="d-flex align-items-center flex-wrap pdetail-r-select">
                    	<?php echo show_add_to_cart_button_quantity($product_id); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <h2 class="s24 text-center pdetail-info-tit mt-5">Thông tin sản phẩm</h2>
                <div class="text-justify pdetail-content wp-editor-fix">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part("resources/views/home-ads"); ?>

<?php get_template_part("resources/views/template-related-product"); ?>

<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
